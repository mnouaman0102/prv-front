import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { ConsultationComponent } from './consultation/consultation.component';
import { FournisseurComponent } from './fournisseur/fournisseur.component';
import { RemiseComponent } from './remise/remise.component';
import {FormsModule} from "@angular/forms";
import {HTTP_INTERCEPTORS, HttpClient, HttpClientModule} from "@angular/common/http";
//import {LoginInterceptor} from "./interceptors/login.interceptor";

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    ConsultationComponent,
    FournisseurComponent,
    RemiseComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [
    // {provide:HTTP_INTERCEPTORS,useClass:LoginInterceptor,multi:true}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
