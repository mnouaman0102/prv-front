import { Injectable } from '@angular/core';
import {Observable} from "rxjs";
import {NumOperation} from "../models/num-operation.model";
import {HttpClient} from "@angular/common/http";
import {Project} from "../models/project.model";
import {Dr} from "../models/dr.model";
import {Site} from "../models/site.model";

@Injectable({
  providedIn: 'root'
})
export class FournisseurService {
  constructor(private httpClient: HttpClient) {}
  getAllNumOperations(): Observable<NumOperation[]> {
    return this.httpClient.get<NumOperation[]>('api/getAllNumOperations');
  }
  getAllProjects():Observable<Project[]>{
    return this.httpClient.get<Project[]>("services/parameters/getAllProjects");
  }

  getAllDr():Observable<Dr[]>{
    return this.httpClient.get<Dr[]>("services/parameters/getAllDr");
  }

  getAllSitesByDr(idtsDr:Array<any>) {
    return this.httpClient.get<Array<Site>>(`services/parameters/getAllSitesByListDr?idtsDr=${idtsDr.join(',')}`);
  }
}
