import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable, tap} from "rxjs";
import {JwtHelperService} from "@auth0/angular-jwt";
import {Router} from "@angular/router";

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
   private urlApi:string='http://localhost:8080/api/v1/auth/login';
  private jwtHelper = new JwtHelperService();

  constructor(private http: HttpClient,private router:Router) {}

  sendLogin(username:string,password:string): Observable<any> {
    let credentials ={ username, password}
    console.log(credentials);
    // @ts-ignore
    return this.http.post(this.urlApi, credentials,{responseType: 'text'});
  }

  logout(): void {
    localStorage.removeItem('token');
    this.router.navigate(['/login'])
  }

  isAuthenticated(): boolean {
    const token = localStorage.getItem('token');
    return !this.jwtHelper.isTokenExpired(token);
  }
}
