export class StringUtil{
  static isNotBlank(str: string | null | undefined): boolean {
    return str != null && str.trim() !== '';
  }
}
