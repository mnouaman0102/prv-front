import {Fournisseur} from "./fournisseur.model";

export class Contrat {

  id:number;
  fournisseur: Fournisseur;
  label:string;
  constructor(id:number,label:string,fournisseur:Fournisseur) {
    this.id=id;
    this.fournisseur=fournisseur;
    this.label=label;

  }
}
