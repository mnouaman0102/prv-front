export class NumOperation {
  id: number;
  numOperation: string;

  constructor(id: number, numOperation: string) {
    this.id = id;
    this.numOperation = numOperation;
  }
  getId() {
    return this.id;
  }

  getNumOperation() {
    return this.numOperation;
  }
}
