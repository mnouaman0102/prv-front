export class Project {
  id: number;
  workflow: string;
  label: string;

  constructor(id: number, workflow: string, label: string) {
    this.id = id;
    this.workflow = workflow;
    this.label = label;
  }

  getId() {
    return this.id;
  }

  getWorkflow() {
    return this.workflow;
  }

  getLabel() {
    return this.label;
  }


}
