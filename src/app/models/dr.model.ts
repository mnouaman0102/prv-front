export class Dr {
  id: number;
  code: string;
  label: string;

  constructor(id: number, code: string, label: string) {
    this.id = id;
    this.code = code;
    this.label = label;
  }

  getId() {
    return this.id;
  }

  getCode() {
    return this.code;
  }

  getLabel() {
    return this.label;
  }
}
