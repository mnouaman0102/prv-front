import { Component } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {AuthenticationService} from "../services/authentication.service";
import {Router} from "@angular/router";
import {StringUtil} from "../util/string-util";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {
  username:string='';
  password:string='';
  flashMessage:string='';
  constructor(private authenticationService:AuthenticationService, private router:Router) {
  }
  connexion(): void {
      if(StringUtil.isNotBlank(this.username) && StringUtil.isNotBlank(this.password))
      this.authenticationService.sendLogin(this.username,this.password).subscribe(
      (response) => {
        console.log(response)
        localStorage.setItem('token', response);
        this.router.navigate(['/']);
      },
      (error) => {
        console.error('Login failed', error);
        this.flashMessage="Bad credentials"
      }
    );
  }
}
