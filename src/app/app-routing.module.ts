import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {LoginComponent} from "./login/login.component";
import {FournisseurComponent} from "./fournisseur/fournisseur.component";
import {RemiseComponent} from "./remise/remise.component";
import {ConsultationComponent} from "./consultation/consultation.component";
import {HomeComponent} from "./home/home.component";
import {authGuard} from "./guards/auth.guard";

const routes: Routes = [
  { path: '', canActivate:[authGuard], component: HomeComponent,children:[
      { path: 'fournisseur', component: FournisseurComponent },
      { path: 'remise-controle', component: RemiseComponent },
      { path: 'consultation', component: ConsultationComponent }
    ] },
  { path: 'login', component: LoginComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
