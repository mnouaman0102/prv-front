import {Component, OnInit} from '@angular/core';
import {NumOperation} from "../models/num-operation.model";
import {FournisseurService} from "../services/fournisseur.service";
import {Project} from "../models/project.model";
import {Site} from "../models/site.model";
import {Dr} from "../models/dr.model";
import {Contrat} from "../models/contrat.model";

@Component({
  selector: 'app-fournisseur',
  templateUrl: './fournisseur.component.html',
  styleUrls: ['./fournisseur.component.css']
})
export class FournisseurComponent implements OnInit{
  numOperations: Array<NumOperation> = []
  projects:Array<Project>=[]
  sites:Site[]=[]
  listDr:Dr[]=[]
  fourniCriDto = {
    idtsContrat: [],
    idtsProjet: [],
    idtsDr: [],
    idtsSite: [],
    idtsEtat: [1],
    numOperation: []
  }
  contrats: Array<Contrat>=[];
  constructor(private fournisseurService:FournisseurService) {
  }
  ngOnInit() {
    // this.loadNumOperations();
    // this.loadProjects();
    // this.loadAllDr();
  }

  changeDR(idtsDr: Array<any>) {
    if (idtsDr != null && idtsDr.length > 0) {
     this.loadAllSitesByDr(idtsDr);
    }
  }

  loadNumOperations(){
     this.fournisseurService.getAllNumOperations().subscribe((data)=>{
       this.numOperations=data;
     },(error)=>{
       console.error("Error loading numOperations",error);
     })
  }
  loadProjects(){
    this.fournisseurService.getAllProjects().subscribe((data)=>{
      this.projects=data;
    },(error)=>{
      console.error("Failed loadig ",error);
    })
  }
  loadAllSitesByDr(idtsDr:Array<any>){
    this.fournisseurService.getAllSitesByDr(idtsDr).subscribe((data) => {
      this.sites = data;
    }, (error) => {
      console.error("Error while loading sites", error);
    });
  }
  loadAllDr(){
    this.fournisseurService.getAllDr().subscribe((data) => {
      this.listDr = data;
    }, (error) => {
      console.error("Error while loading drs", error);
    });
  }
}
